#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 15:32:33 2021

Trading environment to test strategies

@author: jnoble
"""
import matplotlib.pyplot as plt
import strategies as strategies
from create_indicators import load_set
from trade_environment import TradeEnv

# load in training set and initialize trade environment
load_path = '/home/jnoble/Data/algo_trade/training_sets/'
filename = 'tr_set_5stock_2018.pkl'
stocks = load_set(filename, path=load_path)
n_stocks = len(stocks)
env = TradeEnv(stocks, buy_fee=0.001, sell_fee=0.001)

start = 0
end = start + 8760  # look back 1 year
profits = []

# init the environment to start only with USD
env.init_wallet(idx=start, balanced=False)

# loop over range
for m in range(start, end):
    observe = env.observation(m)
    action, stock_num = strategies.mean_accel(env.stocks, observe)
    if action == 0:
        env.buy_stock(stock_num, m)
    elif action == 1:
        env.sell_stock(stock_num, m)
    else:
        # holding, no action to take
        pass

    profits.append(env.wallet_value(m))

best_baseline = env.baseline(start, end)
print(profits[-1])
print(best_baseline)

plt.figure()
plt.plot(profits)
plt.show()
