#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 22:29:37 2021

Automated trading environment to test buys and sells

@author: jnoble
"""
import numpy as np


class TradeEnv:
    def __init__(self, stocks, buy_fee=0.005, sell_fee=0.005):
        self.stocks = stocks
        self.n_stocks = len(stocks)
        self.n_times = len(stocks[0]['indicators'])
        self.buy_fee = buy_fee
        self.sell_fee = sell_fee
        self.wallets = []

    def init_wallet(self, idx, balanced=True):
        """ Initialize the wallet balances
        """
        if balanced:
            closes = [stock['close'][idx] for stock in self.stocks]
            closes.append(1.)
            bals = 1. / ((self.n_stocks+1) * np.array(closes))
            self.wallets = bals
        else:
            bals = [0.] * self.n_stocks
            bals.append(1.)
            self.wallets = bals

    def buy_stock(self, stock_num, idx):
        """ Update wallets for a buy order
        """
        close_usd = self.stocks[stock_num]['close'][idx]
        order_stock = self.wallets[-1] / close_usd * (1-self.buy_fee)
        self.wallets[stock_num] += order_stock
        self.wallets[-1] = 0.

    def sell_stock(self, stock_num, idx):
        """ Update wallets for a sell order
        """
        close_usd = self.stocks[stock_num]['close'][idx]
        order_usd = self.wallets[stock_num] * close_usd * (1-self.sell_fee)
        self.wallets[stock_num] = 0.
        self.wallets[-1] += order_usd

    def observation(self, idx, n_hist=8):
        """ Observation should be current wallet state + indicators with
        history length
        """
        obs = []
        for m in range(self.n_stocks):
            indicator = self.stocks[m]['indicators'][idx-n_hist:idx, :]
            indicator = np.ndarray.flatten(indicator)
            obs.extend(indicator)
        obs.append(np.argmax(self.wallets))
        return obs

    def wallet_value(self, idx):
        """ Get usd value of all wallets
        """
        value_usd = 0.
        for m in range(self.n_stocks):
            value_usd += self.wallets[m] * self.stocks[m]['close'][idx]

        value_usd += self.wallets[-1]
        return value_usd

    def baseline(self, idx0, idx1):
        """ Get best baselines between indices
        """
        base = []
        for stock in self.stocks:
            close0 = stock['close'][idx0]
            close1 = stock['close'][idx1]
            base.append(1 + (close1 - close0) / close0)
        base.append(1.)

        return base
