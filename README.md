# Portfolio
Repo containing some common tools for optimization and network training.

### genetic.py
Genetic algorithm implementation for optimization

### shallow_net.py
Basic feedforward shallow network

### shallow_trainer.py
Trainer for a shallow neural network, set up for classification

### qnet.py
Network and agent for a dueling deep-q network.  Can be used for reinforcement learning.

### qnet_trainer.py
Training environment for reinforcement qnet.  Currently set for a stock trading environment.  Action is which stock is currently in your wallet, or USD.  Reward is the net profit after trade fees.
Sadly stock trading is far to stochastic to do reinforment learning... Was worth a try though.  Overfits so well it's actually impressive :)
