#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 18:36:37 2021

Josh Noble, 2021
"""
import numpy as np
import torch
from torch import nn
from torch import optim


class QNetwork(nn.Module):
    def __init__(self, n_actions, n_states, lr):
        """ Feedforward network architecture
        n_action:  number of possible actions for the agent to take
        n_states:  number of possible states, which entity is filled
        """
        super().__init__()
        self.fc1 = nn.Linear(n_states, 8)
        self.fc2 = nn.Linear(8, 8)
        self.fc3 = nn.Linear(8, 8)
        self.fc4 = nn.Linear(8, 8)
        self.reward = nn.Linear(8, 1)
        self.action = nn.Linear(8, n_actions)

        self.optimizer = optim.Adam(self.parameters(), lr=lr)
        self.loss = nn.MSELoss()
        self.device = torch.device('cuda:1')
        self.to(self.device)

    def forward(self, state):
        """ Forward propagation, relu activations, returns a reward/action
        """
        relu = nn.ReLU()
        x = self.fc1(state)
        x = relu(x)
        x = self.fc2(x)
        x = relu(x)
        x = self.fc3(x)
        x = relu(x)
        x = self.fc4(x)
        x = relu(x)
        rew = self.reward(x)
        act = self.action(x)

        return rew, act


class QAgent:
    def __init__(self, gamma, epsilon, input_dim, batch_size, n_actions,
                 mem_size=10000, eps_min=0.05, swap_interval=500,
                 lr=0.001):
        """ Implementation for a dueling qnet training

        swap_interval:  Swap the network weights every n intervals
        """
        self.gamma = gamma
        self.epsilon = epsilon
        self.eps_min = eps_min
        self.epoch = 0
        self.swap_interval = swap_interval
        self.action_space = [m for m in range(n_actions)]
        self.mem_size = mem_size
        self.batch_size = batch_size
        self.input_dim = input_dim
        self.device = torch.device('cuda:1')
        self._init_memory()

        self.qnet0 = QNetwork(n_actions=n_actions, n_states=input_dim, lr=lr)
        self.qnet1 = QNetwork(n_actions=n_actions, n_states=input_dim, lr=lr)

    def _init_memory(self):
        """ Initialize agent memory
        """
        self.mem_counter = 0

        self.state0_memory = torch.zeros(
            (self.mem_size, self.input_dim),
            dtype=torch.float32
        ).to(self.device)

        self.state1_memory = torch.zeros(
            (self.mem_size, self.input_dim),
            dtype=torch.float32
        ).to(self.device)

        self.action_memory = np.zeros(self.mem_size, dtype=np.int32)

        self.reward_memory = torch.zeros(
            self.mem_size, dtype=torch.float32).to(self.device)

    def store_transition(self, state0, action, reward, state1):
        """ Store state transitions, reward, and actions into memory
        """
        m = self.mem_counter % self.mem_size  # reinit buffer after mem_size
        self.state0_memory[m] = torch.tensor(state0, dtype=torch.float32)
        self.state1_memory[m] = torch.tensor(state1, dtype=torch.float32)
        self.reward_memory[m] = torch.tensor(reward, dtype=torch.float32)
        self.action_memory[m] = action
        self.mem_counter += 1

    def _swap_target_net(self):
        """ Swap the weights from q0 network into q1 network
        """
        if self.mem_counter % self.swap_interval == 0:
            self.qnet1.load_state_dict(self.qnet0.state_dict())

    def choose_action(self, observation, override=False):
        """ Chose an action, either network policy or random
        """
        if self.epsilon < self.eps_min:
            self.epsilon = self.eps_min

        if np.random.random() > self.epsilon or override:
            state = torch.tensor(
                [observation],
                dtype=torch.float32
            ).to(self.device)

            _, action = self.qnet0.forward(state)
            action = torch.argmax(action).item()

        else:
            action = np.random.choice(self.action_space)

        return action

    def _sample_memory(self):
        """ Pull samples from memory buffer
        """
        max_mem = min(self.mem_counter, self.mem_size)
        batch = np.random.choice(max_mem, self.batch_size, replace=False)

        state0_batch = self.state0_memory[batch].clone().detach()
        state1_batch = self.state1_memory[batch].clone().detach()
        reward_batch = self.reward_memory[batch].clone().detach()
        action_batch = self.action_memory[batch]

        return state0_batch, state1_batch, reward_batch, action_batch

    def learn(self):
        """ Learn from the previous and next states
        """
        if self.mem_counter < self.batch_size:
            return

        self.qnet0.optimizer.zero_grad()
        self._swap_target_net()

        state0, state1, reward, action = self._sample_memory()
        action = torch.tensor(action).to(self.device)
        rew0, act0 = self.qnet0.forward(state0)
        rew1, act1 = self.qnet1.forward(state1)

        q_pred = torch.add(
            rew0, (act0 - act0.mean(dim=1, keepdim=True))
        ).gather(1, action.unsqueeze(-1)).squeeze(-1)

        q_next = torch.add(
            rew1, (act1 - act1.mean(dim=1, keepdim=True))
        )

        q_target = reward + self.gamma * torch.max(q_next, dim=1)[0].detach()

        # Bellman equation for reinforcement training loss
        loss = self.qnet0.loss(q_target, q_pred).to(self.device)
        loss.backward()
        self.qnet0.optimizer.step()
