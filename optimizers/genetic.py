#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 15:32:33 2022

Josh Noble, 2022
"""
import time
from copy import copy
import numpy as np
from numpy.random import rand, randint, choice
import matplotlib.pyplot as plt


class GeneticSolver:
    """ Class for genetic algorithm solver

    n_params:  number of parameters to optimize for
    n_bits:  resolution for potential solutions between bounds
    n_tourn:  number of individuals in each tournament selection
    crossover:  crossover point when combining parents
    mutation:  percent of population to mutate
    outsiders:  percent of new blood to inject each generation
    cross_type:  one-point or uniform
    mode:  convex or concave optimization
    """

    def __init__(self,  n_params, n_bits=32, n_tourn=3, crossover=0.3, mutation=0.2, outsiders=0.1,
                 cross_type='one-point', mode='concave'):
        self.n_params = n_params
        self.n_bits = n_bits
        self.n_tourn = n_tourn
        self.crossover = crossover
        self.mutation = mutation * n_params / n_bits
        self.outsiders = outsiders
        self.cross_type = cross_type
        self.mode = mode

    def _selection(self, population, fitness):
        """ Tournament style to see check fitness of population
        """
        n_pop = len(population)
        pop_idx = np.random.randint(n_pop)

        for m in randint(0, n_pop, self.n_tourn-1):
            if fitness[m] < fitness[pop_idx] and self.mode == 'concave':
                pop_idx = m
            elif fitness[m] >= fitness[pop_idx] and self.mode == 'convex':
                pop_idx = m

        return population[pop_idx]

    def _breed(self, parent1, parent2):
        """ Create two children from a pair of parent chromosomes
        """
        # copy to not overwrite parent chromosome on each iteration
        len_chromo = len(parent1)
        child1, child2 = copy(parent1), copy(parent2)

        # one-point crossover randomly select a crossover point between the two parents
        if self.cross_type == 'one-point':
            if rand() < self.crossover:
                idx = randint(1, len_chromo-2)
                child1 = parent1[:idx] + parent2[idx:]
                child2 = parent1[idx:] + parent2[:idx]

        # uniform crossover, select elements from both parents each chosen with equal probability
        elif self.cross_type == 'uniform':
            chromo_vec = np.arange(len_chromo)
            idxs1 = choice(chromo_vec, int(len_chromo * self.crossover)).tolist()
            idxs2 = list(set(chromo_vec) - set(idxs1))
            for m in idxs1:
                child1[m] = parent1[m]
                child2[m] = parent2[m]
            for m in idxs2:
                child1[m] = parent2[m]
                child2[m] = parent1[m]

        else:
            raise ValueError("Unsupported crossover type!")

        return [child1, child2]

    def _mutate(self, chromosomes):
        """ Mutate a base pair in a chromosomes based on probability of mutation,
        in-place operation
        """
        for m in range(len(chromosomes)):
            if rand() < self.mutation:
                chromosomes[m] = 1 - chromosomes[m]

    def _rand_chromosomes(self):
        """ Generate random chromosomes for seeding, base 2
        """
        chromosomes = randint(0, 2, self.n_bits * self.n_params).tolist()
        return chromosomes

    def _decode_chromosomes(self, chromosome, bounds):
        """ Decode a chromosome into a solution of params
        """
        max_val = 2 ** self.n_bits
        params = []

        # each iteration corresponds to 1 parameter
        for m in range(len(bounds)):
            idx1 = m * self.n_bits
            idx2 = (m * self.n_bits) + self.n_bits

            # convert single chromosome from binary list to integer value
            chromo_val = int(''.join([str(m) for m in chromosome[idx1:idx2]]), 2)
            # scale integer value to bounds
            param = bounds[m][0] + chromo_val / max_val * (bounds[m][1] - bounds[m][0])
            params.append(param)

        return params

    def solve(self, objFunc, bounds, n_gens=500, n_pop=100):
        """ Solve the objective function within parameter bounds

        objFunc:  Objective function finding parameters for
        n_gens:  Number of generations
        n_pop:  Population size
        """
        # seed the initial population, best individual, best fitness
        best_chromo = 0
        population = [self._rand_chromosomes() for _ in range(n_pop)]

        # decode the chromosome into parameters and test fitness
        params = self._decode_chromosomes(population[0], bounds)
        best_fitness = objFunc(params)

        for gen in range(n_gens):
            # test fitness of current population
            solutions = [self._decode_chromosomes(chromo, bounds) for chromo in population]
            fitness = [objFunc(sol) for sol in solutions]

            # check for new best solution, either concave or convex
            for m in range(n_pop):
                if fitness[m] < best_fitness and self.mode == 'concave':
                    best_chromo = population[m]
                    best_fitness = fitness[m]
                    print(f"Gen: {gen}  Fitness: {best_fitness}")

                elif fitness[m] >= best_fitness and self.mode == 'convex':
                    best_chromo = population[m]
                    best_fitness = fitness[m]
                    print(f"Gen: {gen}  Fitness: {best_fitness}")

            # select best parents based on fitness
            parents = [self._selection(population, fitness) for _ in range(n_pop)]

            # cross parents to make children population
            children = []
            for m in range(0, n_pop-1, 2):
                child1, child2 = self._breed(parents[m], parents[m+1])
                self._mutate(child1)
                self._mutate(child2)
                children.extend([child1, child2])

            # inject new blood into the population and seed the next generation
            population = children
            for _ in range(int(self.outsiders * n_pop)):
                population.append(self._rand_chromosomes())

        solution = self._decode_chromosomes(best_chromo, bounds)
        return [solution, best_fitness]


if __name__ == '__main__':
    """ Stand-alone testing with simple objective function
    """
    x = np.linspace(0, 10, 1000)
    y_true = 3 * x * np.sin(2 * x + np.exp(0.2))

    def func(p):
        y_target = p[0] * x * np.sin(p[1] * x + np.exp(p[2]))
        mse = np.sum((y_target - y_true)**2)
        return mse

    ga = GeneticSolver(n_params=3, mode='concave', cross_type='one-point')
    func_bounds = [[-10.0, 10.0], [-10.0, 10.0], [-10., 10.]]

    start = time.time()
    p, loss = ga.solve(objFunc=func, bounds=func_bounds)
    end = time.time()

    y_fit = p[0] * x * np.sin(p[1] * x + np.exp(p[2]))

    print(p)
    print(f"Time:  {end-start}")

    plt.close('all')
    fig, ax = plt.subplots()
    ax.plot(x, y_true, label='True')
    ax.plot(x, y_fit, label='Fit')
    ax.legend()
    plt.show()
