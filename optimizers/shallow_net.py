#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 13:08:49 2022

Josh Noble, 2022
"""

import torch.nn as nn


class ShallowNet(nn.Module):
    """ Shallow feed-forward network implementation
    """

    def __init__(self, n_features, drop=0.):
        super().__init__()
        self.fc1 = nn.Linear(n_features, 2*n_features)
        self.fc2 = nn.Linear(2*n_features, 2)
        self._dropout = drop

    def forward(self, x):
        """ Propagate the network forward
        """
        relu = nn.ReLU()
        softmax = nn.Softmax(dim=-1)
        dropout = nn.Dropout(self._dropout)

        x = self.fc1(x)
        x = relu(x)
        x = dropout(x)
        x = self.fc2(x)
        x = softmax(x)  # classification activation

        return x
