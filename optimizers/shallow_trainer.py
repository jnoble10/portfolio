#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 13:20:07 2022

Josh Noble, 2022
"""

import os
import hashlib
import pickle
from copy import copy
import torch
from torch.nn.functional import one_hot
from shallow_net import ShallowNet

os.environ['CUDA_LAUNCH_BLOCKING'] = '1'


def get_lr(optimizer):
    """ Get current learning rate
    """
    for param_group in optimizer.param_groups:
        return param_group['lr']


class ShallowTrainer:
    """ Class for predicting % change in price over later time intervals
    """

    def __init__(self, n_features, filename='shallow_net.pt'):
        self.n_features = n_features
        self.filename = filename
        self._model = ShallowNet(n_features)
        self._state_name = self._get_state_name()
        self._set_device()

    def _get_state_name(self):
        """ Generate a unique state name based on network parameters
        """
        name = f"{self.n_features}"
        state_name = hashlib.sha1(str.encode(name)).hexdigest()
        return state_name

    def _set_device(self):
        """ Set device to GPU training if available
        """
        if torch.cuda.is_available():
            self._device = torch.device('cuda')
            torch.cuda.empty_cache()
            print(torch.cuda.get_device_name(0))
            print('Memory Usage:')
            print("Allocated:",
                  round(torch.cuda.memory_allocated(0)/1024**3, 1), 'GB')
            print("Cached:",
                  round(torch.cuda.memory_reserved(0)/1024**3, 1), 'GB')
        else:
            self._device = torch.device('cpu')
            print("GPU not available, defaulting to cpu")

    def _save_network(self):
        """ Save the trained network as a state dictionary, create new if not available
        """
        try:
            nets = torch.load(self.filename)
        except FileNotFoundError:
            nets = {}

        nets[self._state_name] = self._model.state_dict()
        torch.save(nets, self.filename)

    def _load_network(self):
        """ Load a trained network based on number of features
        """
        try:
            nets = torch.load(self.filename)
            if self._state_name in nets.keys():
                self._model.load_state_dict(nets[self._state_name])
            else:
                raise Exception("No matching model found!  Train new model!")

        except FileNotFoundError:
            print("No file with trained models found!")

    def _next_batch(self, x_set, y_set, batch_size):
        """ Yield the next set of (x,y) data by batch size
        """
        for m in range(0, x_set.size(0), batch_size):
            yield x_set[m: m+batch_size], y_set[m: m+batch_size]

    def train(self, x_set, y_set, val_perc=0.3, batch_size=64, max_epochs=10e3,
              break_counter=100, lr=0.003, decay=1e-4, epsilon=1e-9):
        """ Loop to train the network with associated hyperparameters

        x_set, y_set:  objects and labels
        val_perc:  training-validation split
        """
        # Set the network to train mode and initialize the optimizer and loss
        self._model.train()
        self._model.to(self._device)
        criterion = torch.nn.MSELoss()  # multi-class loss
        # criterion = torch.nn.BCELoss()  # binary loss

        optimizer = torch.optim.AdamW(
            self._model.parameters(),
            lr=lr,
            weight_decay=decay,
            eps=epsilon,
        )

        # split total set into training/validation
        assert len(x_set) == len(y_set)
        n_valid = int(len(x_set) * val_perc)
        n_train = len(x_set) - n_valid

        # move to device, either GPU or CPU
        x_train = x_set[:n_train].to(self._device)
        y_train = y_set[:n_train].to(self._device).float()
        x_valid = x_set[n_train:].to(self._device)
        y_valid = y_set[n_train:].to(self._device).float()
        x_train = x_train[:, None, :]
        y_train = y_train[:, None, :]
        x_valid = x_valid[:, None, :]
        y_valid = y_valid[:, None, :]

        epoch, running_loss = 0, 1e6
        train_losses, valid_losses = [], []

        while epoch < max_epochs:
            epoch += 1

            # training in batched mode
            optimizer.zero_grad()
            tr_loss = 0.
            for x_batch, y_batch in self._next_batch(x_train, y_train, batch_size):
                y_pred = self._model.forward(x_batch)
                loss = criterion(y_pred, y_batch)
                loss.backward()
                optimizer.step()
                tr_loss += loss.detach().to('cpu')

            y_pred = self._model.forward(x_valid)
            loss = criterion(y_pred, y_valid)

            # validation in batched mode
            val_loss = 0.
            for x_batch, y_batch in self._next_batch(x_valid, y_valid, batch_size):
                y_pred = self._model.forward(x_batch)
                loss = criterion(y_pred, y_batch)
                val_loss += loss.detach().to('cpu')

            # store normalized losses for training and validation
            train_losses.append(tr_loss/n_train)
            valid_losses.append(val_loss/n_valid)

            # checkpointing / breaking based on validation loss
            if valid_losses[-1] < running_loss:
                running_loss = copy(valid_losses[-1])
                stop_counter = 0
                self._save_network()
                print(f"Epoch: {epoch}")
                print(f"Train loss: {train_losses[-1]}")
                print(f"Valid loss: {valid_losses[-1]}")
            else:
                stop_counter += 1

            if stop_counter == break_counter:
                print("Validation loss degrading, terminate training")
                break

        return train_losses, valid_losses

    def predict(self, x_set, y_set, batch_size=64):
        """ Prediction/inference in batched mode
        """
        x_set = x_set.to(self._device)
        y_set = y_set.to(self._device).float()
        x_set = x_set[:, None, :]
        y_set = y_set[:, None, :]
        self._load_network()
        self._model.eval()
        self._model.to(self._device)
        y_pred = self._model.forward(x_set)

        # threshold to get accuracy for binary classification
        correct = torch.eq(
            torch.flatten(torch.argmax(y_pred, dim=-1)),
            torch.flatten(torch.argmax(y_set, dim=-1))
        ).sum()

        accuracy = (correct/len(y_pred)).to('cpu').numpy()
        print(f"Accuracy: {accuracy}")

        return accuracy


def load_set(filename, path=''):
    """ Load the pickled machine learning dataset
    """
    file = os.path.join(path, filename)
    with open(file, "rb") as fh:
        ml_set = pickle.load(fh)

    return ml_set


# --------------------------------------------------------------------------
# Stand alone
# --------------------------------------------------------------------------
if __name__ == "__main__":
    import matplotlib.pyplot as plt

    ml_set = load_set(filename='feature_set_10000.pkl')
    x_set = ml_set['samples']
    y_set = ml_set['labels']
    n_features = len(ml_set['features'])

    x_set = torch.tensor(x_set, dtype=torch.float32)
    y_set = one_hot(torch.tensor(y_set, dtype=torch.int64))

    net = ShallowTrainer(n_features)
    tr_loss, val_loss = net.train(
        x_set, y_set,
        batch_size=64, max_epochs=1e4, break_counter=1e3, lr=0.0001
    )

    plt.close('all')
    fig, ax = plt.subplots()
    ax.plot(tr_loss)
    ax.plot(val_loss)
    fig.show()

    acc = net.predict(x_set, y_set)
    print(acc)
