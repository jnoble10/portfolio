#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 9 13:18:33 2021

Josh Noble, 2021
"""
import matplotlib.pyplot as plt
from create_indicators import load_set
from trade_environment import TradeEnv
from q_net import QAgent


load_path = '/home/jnoble/Data/DeepTrade/training_sets/'
filename = 'tr_set_5_2019.pkl'

ml_set = load_set(filename, path=load_path)
stocks = [ml_set[0], ml_set[1], ml_set[2], ml_set[3], ml_set[4]]
n_stocks = len(stocks)
n_indicators = len(stocks[0]['indicators'][0, :])
n_hist = 3


input_dim = n_stocks * n_hist * n_indicators + 1
env = TradeEnv(stocks, buy_fee=0.001, sell_fee=0.001)
agent = QAgent(gamma=0.95, epsilon=1., eps_min=0.01,
               batch_size=1024, n_actions=5,
               input_dim=input_dim, lr=0.003
               )

wallet_vals = []
scores = []
val_scores = []
baselines = []

# start and end time indices to play over
start = 2000
end = 4000

for epoch in range(100):
    # training procedure ####################################################
    score = 0.
    env.init_wallet()
    observation = env.observation(start, n_hist=n_hist)

    # run over 1 time interval
    for idx in range(start, end):
        # get the action, execute in the trade environment, and compute the reward
        action = agent.choose_action(observation)
        env.excecute_trade(idx, action)
        observation_next = env.observation(idx+1, n_hist=n_hist)
        reward = env.reward(idx, idx+1)

        # store the reward and transition, then update the qnetwork
        score += reward
        agent.store_transition(observation, action, reward, observation_next)
        agent.learn()
        observation = observation_next

    # get the baseline balances and update the wallet with the network trade actions
    baselines.append(env.baseline(start, end))
    wallet_vals.append(env.wallet_value(idx))
    scores.append(score)
    agent.epsilon *= 0.95

    print(epoch)
    print(baselines[-1])
    print(wallet_vals[-1])
    print(scores[-1])
    print(agent.epsilon)
    print('\n')

    # do validation every 10 epochs when score is positive
    agent.qnet0.eval()
    if True:
        start_valid = end + 1
        end_valid = 8000
        env.init_wallet()
        score = 0.
        observation = env.observation(start_valid, n_hist=n_hist)

        # validate on time period after the training
        for idx in range(start_valid, end_valid):
            action = agent.choose_action(observation, override=True)
            env.excecute_trade(idx, action)
            observation = env.observation(idx, n_hist=n_hist)
            score += env.reward(idx, idx+1)

        profit = env.wallet_value(idx)
        print(f"Validation: {profit} {score}")
        val_scores.append(scores)
    agent.qnet0.train()


plt.figure()
plt.plot(baselines[-1])
plt.plot(scores)
plt.plot(val_scores)
plt.show()
